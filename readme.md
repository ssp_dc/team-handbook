# SSP team handbook

Its main purpose is to unify how work is done in SSP.

## Installing / Getting started

Just clone the repository and read :).

## Developing

In case you want to improve files in this repository, make a clone and update desired file; if you add new file please also update index.

### Prerequisites

Text editor and some markdown knowledge.

## Style guide

This repository adhere to [this](https://github.com/DavidAnson/markdownlint/blob/master/doc/Rules.md) markdown rules. You can use the markdown liner for VSC.

## Index

* [Git guidelines and workflow](./git.md)
* [Code style for PHP projects](./code-style.md)
* [Writing documentation](./documentation.md)