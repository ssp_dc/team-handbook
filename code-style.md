# Code style

## Laravel

### Some code style guidelines

* Depending on the size of the task use  `//TODO:` comments or open a ticket.
  >_Why:_ So then you can remind yourself and others about a small task (like refactoring a function or updating a comment). For larger tasks use `//TODO(#1234)` which is enforced by a lint rule and the number is an open ticket.

* Always comment and keep them relevant as code changes. Remove commented blocks of code.
  >_Why:_ Your code should be as readable as possible, you should get rid of anything distracting. If you refactored a function, don't just comment out the old one, remove it.

* Avoid irrelevant or funny comments, logs or naming.
  >_Why:_ While your build process may(should) get rid of them, sometimes your source code may get handed over to another company/client and they may not share the same banter.

* Make your names search-able with meaningful distinctions avoid shortened names. For functions use long, descriptive names. A function name should be a verb or a verb phrase, and it needs to communicate its intention.
  >_Why:_ It makes it more natural to read the source code.

* Organize your functions in a file according to the step-down rule. Higher level functions should be on top and lower levels below.
  >_Why:_ It makes it more natural to read the source code.

### Enforcing code style standards

* Use a [this](./samples/.editorconfig.sample) [.editorconfig](http://editorconfig.org/) file which helps developers define and maintain consistent coding styles between different editors and IDEs on the project.
  >_Why:_ The EditorConfig project consists of a file format for defining coding styles and a collection of text editor plugins that enable editors to read the file format and adhere to defined styles. EditorConfig files are easily readable and they work nicely with version control systems.

* Use php-cs-fixer before creating a PR, while using [this](./samples/.php_cs.dist) `.php_cs.dist` file.
  >_Why:_ The php-cs-fixer tool enforces defined set of rules (like @PSR-2) and can auto-apply proposed fixes by itself.
  >**Tip #1:** You can install php-cs-fixer using homebrew
  >```sh
  >brew install php-cs-fixer
  >```
  >**Tip #2**: You can set up Git hooks to apply fixes before commiting a file

## Angular2

* (TBA)